var fontface;
var fontsize;
var c = document.getElementById("mycanvas");
var ctx = c.getContext("2d");
var painting;
var topoff;
var leftoff;
var blackRad = document.getElementById("blackC");
var redRad = document.getElementById("redC");
var yellowRad = document.getElementById("yellowC");
var blueRad = document.getElementById("blueC");
var head;
var bruEra_size = document.getElementById("bru_era_s");
var shaping = false;
var tmp=[];
var startX;
var startY;
var tmpReUn=[];
var count=-1;
var txinp = document.getElementById("txar");
var tx;
var ty;
var monoRad = document.getElementById("monF");
var fantRad = document.getElementById("fanF");
var cursRad = document.getElementById("curF");
var seriRad = document.getElementById("serF");
var text_size = document.getElementById("font_s");
function init(){
    head = -1;
    c.width = 1050;
    c.height = 700;
    ctx.fillStyle = '#FFECB3';
    ctx.fillRect(0, 0, 1050, 700);
    ctx.strokeStyle = '#000000';
    ctx.fillStyle = '#000000';
    topoff = $(c).offset().top;
    leftoff = $(c).offset().left;
    ctx.lineWidth = 3;
    tmpReUn.push(ctx.getImageData(0, 0, 1050, 700));
    count++;
}
c.onmousedown = function(e){
    if(head === 0 || head === 1){
        painting = true;
        ctx.beginPath();
        ctx.moveTo(e.pageX - leftoff, e.pageY - topoff);
    }
    else if(head === 2){
        txinp.style.font = fontsize + " " + fontface;
        txinp.style.display = "block";
        txinp.value = null;
        tx = e.pageX - leftoff + 40;
        ty = e.pageY - topoff + 35;
        txinp.style.left = tx + "px";
        txinp.style.top = ty + "px";
        tx-=20;
        ty-=20;
    }
    else if(head === 3 || head === 4 || head === 5){
        tmp.push(ctx.getImageData(0, 0, 1050, 700));
        shaping = true;
        startX = e.pageX - leftoff;
        startY = e.pageY - topoff;
    }
}
document.onmouseup = function(e){
    if(painting || shaping){
        if(count<tmpReUn.length) tmpReUn.length = count + 1;
        tmpReUn.push(ctx.getImageData(0, 0, 1050, 700));
        count++;
    }
    painting = false;
    shaping = false;
    tmp.pop();
}
c.onmousemove = function(e){
    if(head === 0){
        ctx.strokeStyle = '#FFECB3';
    }
    else if(redRad.checked){
        ctx.strokeStyle = '#FF0000';
        ctx.fillStyle = '#FF0000';
        txinp.style.color = '#FF0000';
    }
    else if(blueRad.checked){
        ctx.strokeStyle = '#0000FF';
        ctx.fillStyle = '#0000FF';
        txinp.style.color = '#0000FF';
    }
    else if(yellowRad.checked){
        ctx.strokeStyle = '#FFFF00';
        ctx.fillStyle = '#FFFF00';
        txinp.style.color = '#FFFF00';
    }
    else if(blackRad.checked){
        ctx.strokeStyle = '#000000';
        ctx.fillStyle = '#000000';
        txinp.style.color = '#000000';
    }
    ctx.lineWidth = bruEra_size.value*3;
    fontsize = text_size.value*10 + "px";
    var x = e.pageX - leftoff;
    var y = e.pageY - topoff;
    if(head === 0 || head === 1){
        ctx.lineCap = 'round';
        if(painting){
            ctx.lineTo(x,y);
            ctx.stroke();
        }
    }
    else if(head ===2){
        if(monoRad.checked){
            fontface = "monospace";
        }
        else if(fantRad.checked){
            fontface = "fantasy"
        }
        else if(cursRad.checked){
            fontface = "cursive"
        }
        else if(seriRad.checked){
            fontface = "serif"
        }
    }
    else if(shaping){
        ctx.putImageData(tmp[0], 0, 0);
        var x2 = e.pageX - leftoff;
        var y2 = e.pageY - topoff;
        if(head === 3){
            powX = Math.pow((x2-startX), 2);
            powY = Math.pow((y2-startY), 2);
            var radius = Math.sqrt((powX+powY), 2);
            ctx.beginPath();
            ctx.arc(startX, startY, radius, 0, Math.PI*2, false);
            ctx.stroke();
            ctx.closePath();
        }
        else if(head === 4){
            ctx.strokeRect(startX, startY, x2-startX, y2-startY);
        }
        else if(head === 5){
            ctx.beginPath();
            ctx.moveTo(startX, startY);
            ctx.lineTo(x2, y2);
            ctx.lineTo(startX*2 - x2, y2);
            ctx.closePath();
            ctx.stroke();
        }
    }   
}
function changeHead(a){
    if(a != 2){
        txinp.style.display = "none";
    }
    if(a === 1){
        c.style.cursor = 'url(brush_icon.ico) 0 30,auto';
    }
    else if(a === 0){
        c.style.cursor = 'url(eraser_icon.ico) 10 25,auto';
    }
    else if(a === 2 || a === 3 || a === 4 || a === 5){
        c.style.cursor = 'url(shapebrush.ico) 10 10,auto';
    }
    head = a;
}
function reset(){
    ctx.clearRect(0,0,1050,700);
    tmpReUn=[];
    tmpReUn.push(ctx.getImageData(0, 0, 1050, 700));
    count = 0;
}
function undo(){
    if(count>0) {
        count--;
        ctx.putImageData(tmpReUn[count], 0, 0)
    }
}
function redo(){
    if(count<tmpReUn.length-1){
        count++;
        ctx.putImageData(tmpReUn[count], 0, 0);
    }
}
function doInput(id){
    var inputObj = document.createElement('input');
    inputObj.addEventListener('change',readFile,false);
    inputObj.type = 'file';
    inputObj.accept = 'image/*';
    inputObj.id = id;
    inputObj.click();
}

function readFile(){
    var file = this.files[0];//獲取input輸入的圖片
    console.log(file.type);
    //if(!/image\/\w /.test(file.type)){
    //alert("請確保檔案為影象型別");
    //return false;
    //}//判斷是否圖片，在移動端由於瀏覽器對呼叫file型別處理不同，雖然加了accept = 'image/*'，但是還要再次判斷
    var reader = new FileReader();
    reader.readAsDataURL(file);//轉化成base64資料型別
    reader.onload = function(e){
    drawToCanvas(this.result);
    }
}
function drawToCanvas(imgData){
    var cvs = document.querySelector('#mycanvas');
    cvs.width=1050;
    cvs.height=700;
    var ctx = cvs.getContext('2d');
    var img = new Image;
    img.src = imgData;
    img.onload = function(){//必須onload之後再畫
    ctx.drawImage(img,0,0,1050,700);
    strDataURI = cvs.toDataURL();//獲取canvas base64資料
    }
}
function windowResize(){
    topoff = $(c).offset().top;
    leftoff = $(c).offset().left;
}
txinp.addEventListener('keydown', function(e){
    if(e.keyCode === 13){
        ctx.font = fontsize + " " + fontface;
        ctx.fillText(txinp.value,tx-20,ty+10);
        txinp.value = null;
        if(count<tmpReUn.length) tmpReUn.length = count + 1;
        tmpReUn.push(ctx.getImageData(0, 0, 1050, 700));
        count++;
        txinp.style.display = "none";
    }
})
init();
