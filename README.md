# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
我的網頁一開始進去是正常鼠標圖案，當使用者按下筆刷圖示按鈕後可以開始畫圖形，按下橡皮擦圖示按鈕後可以擦拭畫布。
BLACK RED BLUE YELLOW選單，按下後可以切換顏色，當你按下後，所有功能的顏色都會變換，包括筆刷、輸入文字和畫圖形。
BRUSH SIZE拉桿，可以調整筆刷大小。
圖示T的按鈕，按下後可以輸入文字，輸入後按下Enter可以將輸入框內文字印到畫布上。
MONOSPACE FANTASY CURSIVE SERIF選單，按下後可以切換輸入的字型。
FONT SIZE拉桿，可以調整文字大小。
圖示圓形 圖示方形 圖示三角形按鈕，按下後可以繪製圖型，分別可以繪製圓形，方形，三角形。
圖示左箭頭 圖示右箭頭按鈕，分別是redo undo 
圖示X按鈕，reset。
圖示上箭頭 圖示下箭頭按鈕，上傳 下載圖片。
